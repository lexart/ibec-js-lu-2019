var usuariosCtrl = function (user) {
	console.log("run usuarios ::", appCont);

	try {
		var user = JSON.parse(window.localStorage.getItem('_jsonUser'));
	} catch (e){
		console.log("catch: ", e);

		window.localStorage.clear();
		window.location.reload();
	}
	

	var VIWES   	= "components/usuarios/views";
	var req	 		= $.get(VIWES + "/indexView.html");
	var WLCM_MSG 	= "Bienvenido";
	var reqMl 		= $.get(MODELS + "usuarios.json");
	var usuario 	= {};
	var usuariosMl 	= [];

	var renderTable = function (arr){
		var tr = "";
		arr.map( function (item, i){
			var button = `<button 
								class="btn btn-primary editar_usuario" 
								id="usr-`+item.id+`">Editar
						  </button>`;
			tr += '<tr>';
				tr += '<td>' + item.id + '</td>';
				tr += '<td>' + item.nombre + '</td>';
				tr += '<td>' + item.email + '</td>';
				tr += '<td>' + button +'</td>';
			tr += '</tr>';
		})

		appCont.find('table tbody').html(tr);

		appCont.find('table').show();
		appCont.find('form').hide();

		appCont.find('form input').val("");
	};

	// VOLVER A LISTADO
	appCont.on('click', '.btn.btn-default', function (ev){
		appCont.find('table').show();
		appCont.find('form').hide();
	})

	// OBTENER USUARIO
	appCont.on('click', '.editar_usuario', function (ev){
		var idAtt = $(this).attr('id');
		var id    = idAtt.split('-')[1];

		console.log("id: ", id);
		console.log("usuariosMl", usuariosMl);

		usuariosMl.map( function (item){
			if(item.id == id){
				usuario = item;
				return false;
			}
		})

		appCont.find('table').hide();
		appCont.find('form').show();

		appCont.find('form').find('[name=id]').val(usuario.id)
		appCont.find('form').find('[name=nombre]').val(usuario.nombre)
		appCont.find('form').find('[name=email]').val(usuario.email)
		appCont.find('form').find('[name=clave]').val(usuario.clave)

		console.log("item find: ", usuario);
	});

	// GUARDAR USUARIO
	appCont.on('click', '.guardar_usuario', function (ev){
		var _formData = {
			id: null,
			nombre: "",
			email: "",
			clave: ""
		};

		_formData.id 		= appCont.find('form').find('[name=id]').val();
		_formData.nombre 	= appCont.find('form').find('[name=nombre]').val();
		_formData.email 	= appCont.find('form').find('[name=email]').val();
		_formData.clave 	= appCont.find('form').find('[name=clave]').val();

		if(_formData.id){
			usuariosMl.map( function (item, i){
				if(item.id == _formData.id){
					usuariosMl[i] = _formData;
					return false;
				}
			})
		} else {
			_formData.id = (Math.random(100, 10) * 100000).toFixed(0);
			usuariosMl.push(_formData);
		}

		window.localStorage.setItem("_jsonUsuarios", JSON.stringify(usuariosMl));

		// RENDER TABLA
		renderTable(usuariosMl);

	});

	// NUEVO USUARIO
	appCont.on('click', '.nuevo_usuario', function (ev){
		appCont.find('table').hide();
		appCont.find('form').show();
	})

	// ELIMINAR USUARIO
	appCont.on('click', '.eliminar_usuario', function (ev){
		var id   = appCont.find('form').find('[name=id]').val();
		console.log("id usuario: ", id);
		if(confirm("Está seguro que desea eliminar este usuario?")){
			console.log("usuario eliminado ::", id);
			usuariosMl.map( function (item, i){
				if(item.id == id){
					usuariosMl.splice(i, 1);
					return false;
				}
			});

			window.localStorage.setItem("_jsonUsuarios", JSON.stringify(usuariosMl));

			// RENDER TABLA 
			renderTable(usuariosMl);
			
		} else {}
		console.log("usuariosMl: ", usuariosMl);
		
	});


	// RENDER USUARIOS
	req.then( function (res){
		appCont.html(res);

		if(user){
			appCont.find('b').text(user.user);
		} else {
			appCont.find('p').text(WLCM_MSG);
		}

		// USUARIOS REQ
		reqMl.then( function (usuarios){
			console.log("usuarios: ", usuarios);
			

			if(!window.localStorage.getItem("_jsonUsuarios")){
				window.localStorage.setItem("_jsonUsuarios", JSON.stringify(usuarios));
			}

			try {
				usuariosMl = JSON.parse(window.localStorage.getItem("_jsonUsuarios"));
			} catch(e){
				usuariosMl = usuarios;
			}

			// RENDER TABLA
			renderTable(usuariosMl);
		});
	});
};