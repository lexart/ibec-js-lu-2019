var appCont = $('#app');

var cerrarSesion = function (){
	appCont.on( 'click', '#cerrarSesion', function () {
		window.localStorage.clear();
		window.location.reload();

		return false;
	})
}
cerrarSesion();

var routes 		= [
	{
		"name": "usuarios",
		"controller":usuariosCtrl
	},
	{
		"name": "dashboard",
		"controller": dashboardCtrl
	},
	{
		"name":"notfound",
		"controller":notfoundCtrl
	}
];


var navMenu 	= function (){
	appCont.on('click', '.nv--link', function (){
		var state 	  = $(this).attr('href').replace('#','');
		var loadState = {};
		var findState = false;

		console.log("loadState :: ", loadState);
		routes.map( function (item){
			if(item.name == state){
				loadState = item;
				findState = true;
				return false;
			}
		})

		if(!findState){
			loadState = {
				"name":"notfound",
				"controller":notfoundCtrl
			}
		}

		if(typeof loadState.controller == "function"){
			loadState.controller();
			window.localStorage.setItem('_loadState', JSON.stringify(loadState))
		} else {
			loginCtrl();
		}
		return false;
	})
};
navMenu();

try {
	var authUser = window.localStorage.getItem('_user');
	if(authUser == "alex"){
		// dashboardCtrl();
		var state = JSON.parse(window.localStorage.getItem('_loadState'));
		if(state){
			console.log("state: ", state);
			routes.map( function (item){
				if(item.name == state.name){
					item.controller();
					return false;
				}
			})
		} else {
			dashboardCtrl();
		}
	} else {
		// RUN LOGIN
		window.localStorage.clear();
		loginCtrl();
	}
} catch(e){
	window.localStorage.clear();
	console.log("e: ", e);
	loginCtrl();
}

